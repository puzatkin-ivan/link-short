<?php

namespace App\Tests\Service;

use App\Repository\LinkRepositoryMock;
use App\Service\GenerateShortLinkService;
use App\Util\GuidGenerator;
use PHPUnit\Framework\TestCase;

class GenerateShortLinkServiceTest extends TestCase
{
    /**
     * @var LinkRepositoryMock
     */
    private $repository;
    /**
     * @var GuidGenerator
     */
    private $generator;
    /**
     * @var string
     */
    private $host;

    protected function setUp(): void
    {
        $this->repository = new LinkRepositoryMock();
        $this->generator = GuidGenerator::getInstance();
        $this->host = 'test.ld';
    }

    public function testGenerateShortLink(): void
    {
        $service = new GenerateShortLinkService($this->repository, $this->generator, $this->host);

        $shortLink = $service->generate('test.com/test_index');
        $link = $this->repository->getLastAdded();
        $expectedShortLink = implode('/', [$this->host, $link->getGuid()]);

        $this->assertEquals($shortLink, $expectedShortLink);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}