<?php

namespace App\Tests\Repository;

use App\Entity\Link;
use App\Repository\LinkRepository;
use App\Util\GuidGenerator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class LinkRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testAdd(): void
    {
        /** @var LinkRepository $repo */
        $repo = $this->entityManager->getRepository(Link::class);

        $link = $repo->add(GuidGenerator::getInstance()->generate(), 'functional.test');

        $this->assertEquals($link, $repo->get($link->getGuid()));
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null;
    }
}