<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 */
class Link
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", unique=true)
     */
    private $link_id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $guid;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $url;

    public function getShortLink(string $host): string
    {
        return implode('/', [$host, $this->guid]);
    }

    public function getLinkId(): int
    {
        return $this->link_id;
    }

    public function setLinkId(int $linkId): self
    {
        $this->link_id = $linkId;

        return $this;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
