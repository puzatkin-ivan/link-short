<?php


namespace App\Service;

use App\Repository\LinkRepositoryInterface;
use App\Util\GuidGenerator;

class GenerateShortLinkService
{
    /**
     * @var LinkRepositoryInterface
     */
    private $storage;

    /**
     * @var GuidGenerator
     */
    private $generator;

    /**
     * @var string
     */
    private $host;

    public function __construct(LinkRepositoryInterface $linkStorage, GuidGenerator $generator, string $host)
    {
        $this->storage = $linkStorage;
        $this->generator = $generator;
        $this->host = $host;
    }

    /**
     * @param string $url
     * @return string - short link
     */
    public function generate(string $url): string
    {
        $guid = $this->generator->generate();
        $link = $this->storage->add($guid, $url);

        return $link->getShortLink($this->host);
    }
}