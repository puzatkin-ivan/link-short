<?php

namespace App\Util;

final class GuidGenerator
{
    private static $generator;

    public static function getInstance(): GuidGenerator
    {
        if (!isset(self::$generator))
        {
            self::$generator = new GuidGenerator();
        }

        return self::$generator;
    }

    private function __construct()
    {
    }

    public function generate(): string
    {
        return uniqid();
    }
}