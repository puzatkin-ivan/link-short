<?php

namespace App\Controller;

use App\Entity\Link;
use App\Repository\LinkRepository;
use App\Repository\LinkRepositoryInterface;
use App\Service\GenerateShortLinkService;
use App\Util\GuidGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GenerateShortLinkController extends AbstractController
{
    /**
     * @var GenerateShortLinkService
     */
    private $generateService;

    public function __construct()
    {
        $shortHost = 'localhost';

        /** @var LinkRepositoryInterface|LinkRepository $linkRepository */
        $linkRepository = $this->getDoctrine()->getRepository(Link::class);

        $this->generateService = new GenerateShortLinkService(
            $linkRepository,
            GuidGenerator::getInstance(),
            $shortHost
        );
    }

    public function generate(Request $request): JsonResponse
    {
        $url = $request->request->get('url');
        if (mb_strlen($url) === 0)
        {
            throw new BadRequestHttpException('Invalid request parameter value');
        }

        $shortLink = $this->generateService->generate($url);
        return $this->json(['link' => $shortLink]);
    }
} 