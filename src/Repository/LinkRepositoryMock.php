<?php

namespace App\Repository;

use App\Entity\Link;

class LinkRepositoryMock implements LinkRepositoryInterface
{
    /**
     * @var array
     */
    private $storage;

    /**
     * @var Link
     */
    private $lastAdded;

    /**
     * LinkRepositoryMock constructor.
     */
    public function __construct()
    {
        $this->storage = [];
    }

    /**
     * @param string $guid
     * @param string $url
     * @return Link
     */
    public function add(string $guid, string $url): Link
    {
        $link = new Link();
        $link->setGuid($guid);
        $link->setUrl($url);

        $this->storage[$guid] = $link;

        $this->lastAdded = $link;
        return $link;
    }

    /**
     * @param string $guid
     * @return Link
     * @throws \Exception
     */
    public function get(string $guid): Link
    {
        if (!isset($this->storage[$guid]))
        {
            throw new \Exception("Link with {$guid} isn't exists.");
        }

        return $this->storage[$guid];
    }

    public function getLastAdded(): Link
    {
        if (!isset($this->lastAdded))
        {
            throw new \OutOfRangeException('Storage is empty.');
        }

        return $this->lastAdded;
    }
}