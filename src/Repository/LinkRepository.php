<?php

namespace App\Repository;

use App\Entity\Link;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Link|null find($id, $lockMode = null, $lockVersion = null)
 * @method Link|null findOneBy(array $criteria, array $orderBy = null)
 * @method Link[]    findAll()
 * @method Link[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LinkRepository extends ServiceEntityRepository implements LinkRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Link::class);
    }

    /**
     * @param string $guid
     * @param string $url
     * @return Link
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function add(string $guid, string $url): Link
    {
        $entity = new Link();
        $entity->setGuid($guid);
        $entity->setUrl($url);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($entity);

        $entityManager->flush();
        return $entity;
    }

    /**
     * @param string $guid
     * @return Link
     * @throws \Exception
     */
    public function get(string $guid): Link
    {
        $link = $this->findOneBy(['guid' => $guid]);

        if (!$link)
        {
            throw new \Exception("Link with {$guid} isn't exists.");
        }

        return $link;
    }
}
