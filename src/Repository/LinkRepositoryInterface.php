<?php


namespace App\Repository;

use App\Entity\Link;

interface LinkRepositoryInterface
{
    public function add(string $guid, string $url): Link;
    public function get(string $guid): Link;
}